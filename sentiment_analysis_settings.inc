<?php

/**
 * @file
 * Provides API key for sentiment analysis.
 */

/**
 * Implements hook_form().
 */
function sentiment_analysis_form($form, $form_state) {
  $api_url = variable_get('api_url');
  $api_key = variable_get('api_key');
  $form['get_key'] = array(
    '#markup' => '<a href="https://www.havenondemand.com/login.html">Click Here to get API key.</a>',
  );
  $form['api_url'] = array(
    '#type' => 'textfield',
    '#title' => 'API URL',
    '#default_value' => isset($api_url) ? $api_url : '',
    '#description' => t('API URL for sentiment analysis'),
    '#required' => 'TRUE',
  );
  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'API Key',
    '#default_value' => isset($api_key) ? $api_key : '',
    '#description' => t('API key for sentiment analysis'),
    '#required' => 'TRUE',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function sentiment_analysis_form_submit($form, &$form_state) {
  // To save API key.
  variable_set('api_url', $form_state['values']['api_url']);
  variable_set('api_key', $form_state['values']['api_key']);
  drupal_set_message(t('API key successfully updated'));
}
